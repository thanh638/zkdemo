package controller;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;

import model.LanguageContribution;

public class SimpleRenderer implements RowRenderer<LanguageContribution> {

	public void render(Row row, LanguageContribution data, int index) throws Exception {
		row.appendChild(new Label(Integer.toString(index)));
		row.appendChild(new Label(data.getLanguage()));
		row.appendChild(new Label(data.getName()));
		row.appendChild(new Label(data.getCharset()));
		
		final Div d = new Div();
		final Button thumbBtn = new Button(null, "/images/thumb-up.png");
		thumbBtn.setParent(d);
		thumbBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			public void onEvent(Event arg0) throws Exception {
				d.appendChild(new Label("Thumbs up"));
                thumbBtn.setDisabled(true);
			}
		});
		row.appendChild(d);
	}

}
