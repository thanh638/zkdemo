package model;

public class Mail {

	private int index;
	private String subject;
	private String date;
	private int size;
	
	public Mail() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Mail(int index, String subject, String date, int size) {
		super();
		this.index = index;
		this.subject = subject;
		this.date = date;
		this.size = size;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	
}
