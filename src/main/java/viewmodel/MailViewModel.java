package viewmodel;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;

import model.InboxData;
import model.Mail;

public class MailViewModel {

	InboxData mailData = new InboxData();

	public InboxData getMailData() {
		return mailData;
	}

	public void setMailData(InboxData mailData) {
		this.mailData = mailData;
	}
	
	@Command
	@NotifyChange("mailData")
	public void revertMail() {
		mailData.revertDeletedMails();
	}
	
	@Command
    @NotifyChange("mailData")
	public void deleteAllMail() {
		mailData.deleteAllMails();
	}
	
	@Command
    @NotifyChange("mailData")
    public void removeMail(@BindingParam("mail") Mail myMail) {
        mailData.deleteMail(myMail);
    }
}
