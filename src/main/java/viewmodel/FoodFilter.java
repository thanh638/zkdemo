package viewmodel;

public class FoodFilter {

	private String category = "";
	private String name = "";
	private String nutrients = "";
	
	/*public FoodFilter() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FoodFilter(String category, String name, String nutrients) {
		super();
		this.category = category;
		this.name = name;
		this.nutrients = nutrients;
	}*/
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category==null?"":category.trim();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name==null?"":name.trim();
	}
	public String getNutrients() {
		return nutrients;
	}
	public void setNutrients(String nutrients) {
		this.nutrients = nutrients==null?"":nutrients.trim();
	}
	
}
